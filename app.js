var fs = require('fs')
    , static = require('node-static')
    , http = require('http')
    , socketio = require('socket.io')
    , game = require('./Game.js');

var file = new(static.Server)(webroot, {
  cache: 600,
  headers: { 'X-Powered-By': 'node-static' }
});

var webroot = '',
  port = 31168;

var server = http.createServer(function(req, res) {
  req.addListener('end', function() {
    file.serve(req, res, function(err, result) {
      if (err) {
        console.error('Error serving %s - %s', req.url, err.message);
        if (err.status === 404 || err.status === 500) {
        } else {
          res.writeHead(err.status, err.headers);
          res.end();
        }
      } else {
        console.log('%s - %s', req.url, res.message);
      }
    });
  });
}).listen(port, '127.0.0.1');

var io = socketio.listen(server);
game.io = io;
game.startGame();
io.on('connection', function (socket) {
  console.log("on connect");
  socket.on('nick', function(nick) {
    console.log("on nick");
    var player = game.addToTeam(nick, socket);
  });
});

