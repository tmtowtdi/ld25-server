var fs = require('fs'),
    map = JSON.parse(fs.readFileSync(__dirname + '/map.txt','utf8'));

function Game() {
  this.map = map;
  this.offense = "wolves";
  this.turnCreeps = [];
  this.turnTowers = [];
  this.state = "prep";
  this.towerPositions= {};
  this.numP = 0;
  this.gamePaused = true;
  this.disconnectedPlayers = [];
  this.gameStopped = true;
  this.teamsInitialized = false;
}

Game.prototype.damageBase = function(creep) {
  var team = this.getOppositeTeamObject(creep.team);
  team.damage(creep.damage);
}

Game.prototype.initializeTeams = function() {
  this.teamGoats = new Team();
  this.teamGoats.name = "goats";
  this.teamWolves = new Team();
  this.teamWolves.name = "wolves";
  this.teamsInitialized = true;
}

Game.prototype.startGame = function() {
  if(!this.teamsInitialized)
    this.initializeTeams();
  var game = this;
  this.teamGoats.clearForNewGame();
  this.teamWolves.clearForNewGame();
  this.teamGoats.addTower(game.map[0]);
  this.teamWolves.addTower(game.map[game.map.length-1]);
  this.towerPositions = {};
  this.gameStopped = false;

  function mainLoop() {
    if(game.gameStopped)
    {
      clearInterval(this.mainLoopInterval);
      game.startGame();
    }
    //check if game needs to be paused
    if(game.teamGoats.players.length > 0 && game.teamWolves.players.length > 0)
      game.continueGame();
    else
      game.pauseGame();

    //rounds finished
    if(!game.gamePaused && game.readyForTheRound())
    {
      game.startTurn();
    }
  }

  console.log('setting main loop');
  this.mainLoopInterval = setInterval(mainLoop, 5000);
}

Game.prototype.readyForTheRound =function () {
  if(!this.turnCreeps || this.turnCreeps.length == 0)
  {
    //console.log('no creep in this turn');
    return true;
  }
  var alive = false;
  for(var i in this.turnCreeps)
  {
    alive = alive || this.turnCreeps[i].alive;
  }
  //console.log('is there any alive creep in this turn', alive);
  return !alive;
}

Game.prototype.getOppositeTeamObject = function(teamname) {
  return teamname == "goats" ? this.teamWolves : this.teamGoats;
}

Game.prototype.getTeamObject = function(teamname) {
  return teamname == "goats" ? this.teamGoats : this.teamWolves;
}

Game.prototype.pauseGame = function() {
  this.gamePaused = true;
  //this.broadcast('game paused',true);
}

Game.prototype.continueGame = function() {
  //if(!this.gamePaused) return
  this.gamePaused = false;
  /*
  this.broadcast('game paused',false);
  this.startTurn();
  */
}

Game.prototype.getTurnData = function() {
  return {
    creeps :this.turnCreeps,
    towers : this.turnTowers,
    goatsHealth: this.teamGoats.baseHealth,
    wolvesHealth: this.teamWolves.baseHealth
  }
}

Game.prototype.startTurn = function() {
  this.swapSides();
  var offenseTeam = this.offense == "goats" ? this.teamGoats : this.teamWolves;
  var defenseTeam = this.offense == "goats" ? this.teamWolves : this.teamGoats;
  if(defenseTeam.lastCreepIndex == 0) 
  {
    this.swapSides();
    return;
  }
  this.clearTurnData();
  this.turnCreeps = defenseTeam.getCreepsForTurn();
  this.turnTowers = offenseTeam.getTowersForTurn();
  offenseTeam.emitNewRoundData(this);
  defenseTeam.emitNewRoundData(this);
}

Game.prototype.clearTurnData = function() {
  if(this.turnTowers)
    for(var i in this.turnTowers)
      this.turnTowers[i].die();
  if(this.turnCreeps)
    for(var i in this.turnCreeps)
      this.turnCreeps[i].die();
}

Game.prototype.swapSides = function() {
  this.offense = this.offense == "goats" ? "wolves" : "goats";
}

Game.prototype.killCreep = function(creep) {
  this.broadcast('creep died', creep.id);
}

Game.prototype.getTeam = function(team) {
  if(team == "goats")
    return this.teamGoats;
  else
    return this.teamWolves;
}

Game.prototype.canAddTower = function(tower) {
  return (!(tower in this.towerPositions));
}

Game.prototype.disconnectPlayer = function(player) {
  this.numP--;
  this.disconnectedPlayers.push(player);
  player.leaveTeam();
}

Game.prototype.broadcast = function(message, data) {
  this.io.sockets.emit(message, data);
}

Game.prototype.broadcastToTeam = function(message, data, team)
{
  this.io.sockets.emit(message, data);
}

Game.prototype.addToTeam = function(nick, socket) {
  var pl = null;

  for(var i in this.disconnectedPlayers)
    if(nick == this.disconnectedPlayers[i].nick) {
      pl = this.disconnectedPlayers.splice(i,1)[0];
      break;
    }

  if(!pl)
    pl = new Player();

  if(this.teamGoats.players.length > this.teamWolves.players.length)
    this.teamWolves.addPlayer(pl);
  else
    this.teamGoats.addPlayer(pl);

  this.numP++;
  pl.setup(socket, nick);

  return pl;
}

Game.prototype.upgradeCreep = function(creep, stat) {
  if(!(stat in creepUpgradeProperties))
    return false;
  var val = creepUpgradeProperties[stat](creep[stat]);
  if(null == val)
    return false;
  creep[stat] = val;
  return true;
}

Game.prototype.upgradeTower = function(tower, stat) {
  if(!(stat in towerUpgradeProperties))
    return false;
  var val = towerUpgradeProperties[stat](tower[stat]);
  if(null == val)
    return false;
  tower[stat] = val
  return true;
}

Game.prototype.finalizeGame = function(team) {
  this.broadcast('game finished', {
    loser: team.name
  });
  this.gameStopped = true;
  this.clearTurnData();
}

var creepUpgradeProperties = {
  'speed' : function(val) {
    val -= 100;
    if(val < 200)
      return null;
    return val;
  },
  'health': function(val) {
    val++;
    if(val > 100)
      return null;
    return val;
  },
  'damage': function(val) {
    val++;
    if(val > 100)
      return null;
    return val;
  }
}
var towerUpgradeProperties = {
  'speed' : function(val) {
    val -= 100;
    if(val < 200)
      return null;
    return val;
  },
  'range': function(val) {
    val++;
    if(val > 10)
      return null;
    return val;
  },
  'damage': function(val) {
    val++;
    if(val > 100)
      return null;
    return val;
  }
}
module.exports = new Game();
var Team = require('./Team.js')
   ,Player = require('./Player.js');
