module.exports = Tower;
var game = require('./Game.js');
function Tower () {
  this.damage = 1;
  this.range = 2;
  this.speed = 1500;
  this.x = 0;
  this.y = 0;
  this.id = 0;
  this.alive = false;
}

Tower.prototype.inRange = function(object) {
  var dx = this.x - object.x;
  var dy = this.y - object.y;
  return Math.sqrt(dx*dx + dy*dy) <= this.range;
}

Tower.prototype.doDamage = function(creep) {
  creep.health -= this.damage;
  //console.log('hitting to some creep. remaining hp: ', creep.health);
  if(creep.health <= 0)
    creep.kill();
}

Tower.prototype.startTurn = function() {
  var tower = this;
  //console.log("tower assembled", tower.x, tower.y);
  function hitter() {
    var targets = game.turnCreeps;
    //console.log("tower assembled", tower.x, tower.y);
    for(var ci in targets) {
      var creep = targets[ci];
      if(!creep.alive) 
        continue;
      if(tower.inRange(creep)) {
        //console.log("creeep in sight");
        tower.doDamage(creep);
        break;
      }
    }
  }
  this.hitterInterval = setInterval(hitter, this.speed);
}

Tower.prototype.die = function() {
  clearInterval(this.hitterInterval);
}

Tower.prototype.clone = function() {
  var clone = new Tower();
  var fields = ["speed", "damage", "range", "x", "y", "id"];
  for(var i in fields)
    clone[fields[i]] = this[fields[i]];
  return clone;
}

