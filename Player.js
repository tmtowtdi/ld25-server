var fs = require('fs')
   ,game = require('./Game.js');

module.exports = Player;

function Player() {
  this.gold = 4;
}

Player.prototype.leaveTeam = function(){
  //console.log("trying to left the team");
  var players = this.teamRef.players;
  for(var i in players)
    if(players[i].nick == this.nick)
    {
      //console.log("left the team");
      this.teamRef.players.splice(i,1);
    }
}

Player.prototype.setup = function(socket, nick){
  this.socket = socket;
  this.nick = nick;
  this.socket.join(this.team);
  this.setListeners();
}

Player.prototype.setListeners = function() {
  var player = this;
  player.socket.join(player.team);
  player.sendInitialData();
  player.on("add tower", function (tower) {
    for(var i in game.map) 
      if(game.map[i].x == tower.x && game.map[i].y == tower.y)
      {
        player.emit('message', 'oraya olmaz pamps');
        return;
      }
    /*
    var teamTowers = this.teamRef.towers;
    var isInAnyOfRanges = false;
    for(var i in teamTowers)
      if(teamTowers[i].inRange(tower))
      {
        isInAnyOfRanges = true;
        break;
      }

    if(!isInAnyOfRanges)
    {
      player.emit('message', 'oraya olmaz pamps');
      return;
    }
    */

    if(game.offense == player.team) {
      player.emit('message', 'sirani bekle it');
      return;
    }

    if(game.canAddTower(tower) || player.trySpendGold()) {
      var newTower = player.teamRef.addTower(tower)
      game.towerPositions[tower] = 1;
      if(!newTower)
        player.emit('message', 'olmadi pamps');
      else
      {
        player.broadcast('added tower', {tower:newTower});
        player.spendGold();
      }
    }
  });
  player.on('upgrade tower', function (tower) {
    if(player.trySpendGold()) {
      var upgradedTower = player.teamRef.upgradeTower(tower.id, tower.stat);
      if(null == upgradedTower)
        player.emit('message', 'olmadi pamps');
      else
      {
        player.broadcast('upgraded tower', {tower:upgradedTower});
        player.spendGold();
      }
    }
  });
  player.on('add creep', function (creep) {
    if(game.offense != player.team) {
      player.emit('message', 'sirani bekle it');
      return;
    }
    if(player.trySpendGold()) {
      var newCreep = player.teamRef.addCreep();
      if(!newCreep)
        player.emit('message', 'olmadi pamps');
      else
      {
        player.broadcast('added creep', {creep: newCreep});
        player.spendGold();
      }
    }
  });
  player.on('upgrade creep', function (creep) {
    if(player.trySpendGold())
    {
      var upgradedCreep = player.teamRef.upgradeCreep(creep.id, creep.stat);
      if(null == upgradedCreep)
        player.emit('message', 'olmadi pamps');
      else
      {
        player.broadcast('upgraded creep', {creep:upgradedCreep});
        player.spendGold();
      }
    }
  });
  player.on('disconnect', function() {
    game.disconnectPlayer(player);
  });
}

Player.prototype.notEnoughGold = function() {
  this.emit('message', 'not enough gold');
}

Player.prototype.on = function(message, fn) {
  this.socket.on(message, function(data) {
    //console.log(message, 'geldi');
    fn(data);
  });
}

Player.prototype.broadcast = function(message, data) {
  data.player = this.nick
  //console.log("from broadcast", this.team, data);
  game.io.sockets.in(this.team).emit(message, data);
}

Player.prototype.emit = function(message, data) {
  this.socket.emit(message, data);
}

Player.prototype.sendInitialData = function() {
  this.emit('initial data', {
    mode: game.offense == this.team ? "offense" : "defense",
    gold: this.gold,
    map: game.map,
    team: this.teamRef.getState(),
    turn: game.getTurnData()
  });
}

Player.prototype.trySpendGold = function() {
  if(this.gold > 0) {
    return true;
  }
  this.notEnoughGold();
  return false;
}

Player.prototype.spendGold = function() {
  if(this.gold > 0) {
    this.gold--;
    return true;
  }
  this.notEnoughGold();
  return false;
}

Player.prototype.setGoldForNewTurn = function () {
  this.gold += 4;
}
