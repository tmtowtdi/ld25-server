var game = require('./Game.js');
module.exports = Creep;
function Creep () {
  this.speed = 1500;
  this.health = 1;
  this.damage = 1;
  this.team = "";
  this.x = -100;
  this.y = -100;
  this.id = 0;
  this.mapIndex = 0;
  this.alive = false;
}

Creep.prototype.die = function() {
  clearInterval(this.moveInterval);
}

Creep.prototype.kill = function() {
  this.alive = false;
  clearInterval(this.moveInterval);
  //console.log(this.id, 'creep died');
}

Creep.prototype.startTurn = function(mapind) {
  var creep = this;
  creep.alive = true;
  //console.log( mapind);
  creep.mapIndex = mapind;
  function move() {
    //console.log('trying to move', creep.team, creep.mapIndex);
    if(creep.team == "goats") {
      //console.log('going right');
      creep.mapIndex++;
      if(creep.mapIndex >= game.map.length)
        creep.explode();
    }
    else {
      //console.log('going going left');
      creep.mapIndex--;
      if(creep.mapIndex < 0)
        creep.explode();
    }
    creep.updatePosition();
    //console.log(creep.mapIndex, creep.x, creep.y, creep.alive);
  }
  this.moveInterval = setInterval(move, this.speed);
}

Creep.prototype.updatePosition = function() {
  if(this.mapIndex > 0 && this.mapIndex < game.map.length)
  {
    this.x = game.map[this.mapIndex].x
    this.y = game.map[this.mapIndex].y
  }
}

Creep.prototype.explode = function() {
  game.damageBase(this);
  console.log('explode');
  this.kill();
}

Creep.prototype.clone = function() {
  var clone = new Creep();
  var fields = ["speed", "health", "damage", "team"];
  for(var i in fields)
    clone[fields[i]] = this[fields[i]];
  return clone;
}
