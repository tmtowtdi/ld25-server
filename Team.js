var Tower = require('./Tower.js')
  ,Creep = require('./Creep.js')
  ,game = require('./Game.js');
var CREEPLIMIT = 100;
var TOWERLIMIT = 100;
function Team() {
  this.creeps = []
  this.towers = []
  this.players = []
  this.lastCreepIndex = 0;
  this.lastTowerIndex = 0;
  this.name = "";
  this.baseHealth = 100;
}

Team.prototype.addPlayer = function(player) {
  this.players.push(player)
  player.team = this.name;
  player.teamRef = this
}

Team.prototype.emitNewRoundData = function(game) {
  for(var i in this.players)
  {
    this.players[i].setGoldForNewTurn();
    this.players[i].emit('new round', {
      mode: game.offense == this.name ? "offense" : "defense",
      gold: this.players[i].gold,
      team: this.getState(),
      turn: game.getTurnData()
    });
  }
}

Team.prototype.getState = function() {
  return {
    name: this.name,
    towers: this.towers.slice(0, this.lastTowerIndex),
    creeps: this.creeps.slice(0, this.lastCreepIndex)
  };
}

Team.prototype.clearForNewGame = function() {
  this.creeps = []
  for(var i = 0; i < CREEPLIMIT; ++i)
  {
    var cr = new Creep();
    cr.id = i;
    this.creeps.push(cr);
  }
  this.towers = []
  for(var i = 0; i < TOWERLIMIT; ++i)
  {
    var tw = new Tower();
    tw.id = i;
    this.towers.push(tw);
  }
  this.lastTowerIndex = 0;
  this.lastCreepIndex = 0;
  this.baseHealth = 100;
}

Team.prototype.getTowersForTurn = function() {
  var tws = [];
  for(var i = 0; i < this.lastTowerIndex; ++i)
  {
    var tower = this.towers[i].clone();
    tower.startTurn();
    tws.push(tower);
  }
  return tws;
}

Team.prototype.getCreepsForTurn = function() {
  var crs = [];
  for(var i = 0; i < this.lastCreepIndex; ++i)
  {
    var creep = this.creeps[i].clone()
    if(this.name == "goats")
      creep.startTurn(-i);
    else
      creep.startTurn(game.map.length + i - 1);
    crs.push(creep);
  }
  return crs;
}

Team.prototype.addCreep = function() {
  if(this.lastCreepIndex >= CREEPLIMIT)
    return null;
  var cr = this.getNewCreep();
  cr.team = this.name;
  cr.alive = true;
  return cr;
}

Team.prototype.damage = function(damage) {
  this.baseHealth -= damage;
  if(this.baseHealth < 0)
  {
    game.finalizeGame(this);
  }
}

Team.prototype.upgradeCreep = function(id, stat) {
  var result = game.upgradeCreep(this.creeps[id], stat);
  if(!result)
    return null;
  return this.creeps[id];
}

Team.prototype.upgradeTower = function(id, stat) {
  var result = game.upgradeTower(this.towers[id], stat);
  if(!result)
    return null;
  return this.towers[id];
}

Team.prototype.addTower = function(tower) {
  if(this.lastTowerIndex >= CREEPLIMIT)
    return null;
  var tw = this.getNewTower();
  tw.alive = true;
  tw.x = tower.x;
  tw.y = tower.y;
  return tw;
}

Team.prototype.getNewCreep = function() {
  var cr = this.creeps[this.lastCreepIndex++];
  cr.alive = true;
  return cr;
}

Team.prototype.getNewTower = function() {
  var tw = this.towers[this.lastTowerIndex++];
  return tw;
}

module.exports = Team;
